package ru.dol.modbus.installer;

import lombok.SneakyThrows;

import java.io.*;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.URL;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Objects;

public class Application {

    public static void main(String[] args) {
        if (args.length == 0) {
            startCommandLine();
        } else {
            startUI();
        }
    }

    private static void startUI() {

    }

    private static void startCommandLine() {
        InstallerPojo pojo = new InstallerPojo();

        System.out.println();
        var pathIsCorrect = "";
        while (pathIsCorrect != null) {

            var answer = readLine("Please specify installation path: ");
            pathIsCorrect = verifyInstallationPath(answer);
            if (pathIsCorrect != null) {
                System.out.println(pathIsCorrect);
            }
            pojo.setInstallationPath(answer);
        }


//        return;
        var port = -1;

        while (port == -1) {
            port = checkPort(readLine("Please specify iPort: "));
            pojo.setPort(port);
        }

        boolean correct = false;

        while (!correct) {
            var def = new InstallerPojo();
            String user = readLine("Please enter database user name \"Enter\" to set [" + def.getDatabaseUser() + "] ", def.getDatabaseUser());
            String password = readLine("Please enter database user password ", def.getDatabasePassword());
            String database = readLine("Please enter database name name enter to set [" + def.getDatabase() + "] ", def.getDatabase());
            String databasePort = readLine("Please enter database Port name enter to set [" + def.getDatabasePort() + "] ", def.getPort() + "");
            if (Objects.equals(databasePort, "null")) {
                databasePort = def.getDatabasePort() + "";
            }
            String databaseHost = readLine("Please enter database host name enter to set [" + def.getDatabaseHost() + "] ", def.getDatabaseHost());
            String dialect = readLine("Please enter database dialect name enter to set [" + def.getDialect() + "] ", def.getDialect());
            String driver = readLine("Please enter database driver name enter to set [" + def.getDriverClass() + "] ", def.getDriverClass());
            String url = readLine("Please enter database url enter to set [" + def.getDatabaseUrl() + databaseHost + ":" + databasePort + "/" + database + "] ", def.getDatabaseUrl() + databaseHost + ":" + databasePort + "/" + database);

            correct = true;//tryToConnect(user, password, url, driver);
            pojo.setDatabase(database);
            pojo.setDatabasePassword(password);
            pojo.setDatabasePort(Integer.parseInt(databasePort));
            pojo.setDatabaseHost(databaseHost);
            pojo.setDatabaseUrl(url);
            pojo.setDatabaseHost(databaseHost);
            pojo.setDialect(dialect);
            pojo.setDriverClass(driver);

        }

        var distrUrl = "http://dennis.systems/releases/usb/";
        var distrPath = distrUrl + "usb-1.0-SNAPSHOT.jar";
        var distrI18PathEn = distrUrl + "i18n/messages_en_US.properties";
        var distrI18PathDe = distrUrl + "i18n/messages_de_DE.properties";
        download(distrPath, pojo.getInstallationPath() + FileSystems.getDefault().getSeparator() + "application.jar");
        var translations = new File(pojo.getInstallationPath() + FileSystems.getDefault().getSeparator() + "/i18n/");
        translations.mkdir();

        download(distrI18PathEn, translations.getAbsolutePath() + FileSystems.getDefault().getSeparator() + "messages_en_US.properties" );
        download(distrI18PathDe, translations.getAbsolutePath() +FileSystems.getDefault().getSeparator() + "messages_de_DE.properties");

        try {
            pojo.toProperties().store(new FileOutputStream(pojo.getInstallationPath() + FileSystems.getDefault().getSeparator() + "application.properties"), "");
        } catch (IOException e ) {

            System.out.println("Could not save settings file to the installation folder!");
            e.printStackTrace();
        }

        try {
            Files.writeString(Paths.get(pojo.getInstallationPath() + FileSystems.getDefault().getSeparator() + "run.sh"), "java -jar application.jar --spring.config.location=application.properties");
            Files.writeString(Paths.get(pojo.getInstallationPath() + FileSystems.getDefault().getSeparator() + "run.bat"), "java -jar application.jar --spring.config.location=application.properties");
        } catch (IOException e) {
            System.out.println("Cannot save run config, but you can still run it manually");
            e.printStackTrace();
        }

    }

    @SneakyThrows
    private static boolean tryToConnect(String user, String password, String url, String driver) {

        Class.forName(driver.trim());
        try (var conn = DriverManager.getConnection(url, user, password)) {
            System.out.println("Connections successful to:" + url);
            return true;

        } catch (SQLException e) {
            System.out.println(" connection failed: " + e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

    public static int checkPort(String prt) {

        int port = Integer.parseInt(prt);

        if (port < 10 || port > 99999) {
            return -1;
        }

        ServerSocket ss = null;
        DatagramSocket ds = null;
        try {
            ss = new ServerSocket(port);
            ss.setReuseAddress(true);
            ds = new DatagramSocket(port);
            ds.setReuseAddress(true);
            return port;
        } catch (IOException e) {
            System.out.println("cannot use port: " + prt);
        } finally {
            if (ds != null) {
                ds.close();
            }

            if (ss != null) {
                try {
                    ss.close();
                } catch (IOException e) {
                    /* should not be thrown */
                }
            }
        }

        return -1;

    }

    private static String verifyInstallationPath(String answer) {
        try {
            File file = new File(answer);
            if (!file.exists()) {
                return "Path is not existing, please try again";
            }

            if (!file.isDirectory()) {
                return "Path is not directory but should be directory";
            }

            try {
                var res = new File(answer + FileSystems.getDefault().getSeparator() + ".lock").createNewFile();
                new File(answer + FileSystems.getDefault().getSeparator() + ".lock").delete();

                if (!res) {
                    return "Not enough permissions / space cannot create .lock file in the folder";
                }
            } catch (Exception e) {
                return "Not enough permissions / space cannot create .lock file in the folder";
            }
            return null;
        } catch (Exception e) {
            return e.getMessage();
        }

    }

    public static String readLine(String s) {
        return readLine(s, "");
    }

    public static void download(String url, String s) {

        try (BufferedInputStream in = new BufferedInputStream(new URL(url).openStream());

             FileOutputStream fileOutputStream = new FileOutputStream(s)) {
            byte[] dataBuffer = new byte[1024];
            int bytesRead;
            int i = 0;
            while ((bytesRead = in.read(dataBuffer, 0, 1024)) != -1) {
                i++;
                if (i % 5000 == 0)
                    System.out.print(" . ");
                fileOutputStream.write(dataBuffer, 0, bytesRead);
            }
            System.out.println();
            System.out.println("download complete");
        } catch (IOException e) {
            System.out.println("Error on download: " + e.getMessage());
        }
    }

    @SneakyThrows
    public static String readLine(String s, String def) {
        System.out.println(s);
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(System.in));

        // Reading data using readLine
        String name = reader.readLine();

        if (Objects.equals(name, "")) {
            name = def;
        }

        // Printing the read line
        System.out.println(name);


        return name;
    }
}
