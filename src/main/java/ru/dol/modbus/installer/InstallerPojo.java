package ru.dol.modbus.installer;

import lombok.Data;
import java.io.Serializable;
import java.nio.file.FileSystems;
import java.util.Properties;

@Data
public class InstallerPojo  implements Serializable {

    private Integer port;

    private String installationPath;

    private String database ="usb";

    private String databaseUser = "postgres";

    private String databasePassword;

    private int databasePort = 5432;

    private String databaseHost = "localhost";

    private String databaseUrl = "jdbc:postgresql://";

    private String driverClass = "org.postgresql.Driver";

    private String dialect = "org.hibernate.dialect.PostgreSQLDialect";



    private Boolean runAsService;

    public Properties toProperties() {
        Properties properties = new Properties();
        properties.setProperty("server.port", getPort() + "");
        properties.setProperty("spring.datasource.database", getDatabase());
        properties.setProperty("spring.datasource.password", getDatabasePassword() == null ? "" : getDatabasePassword());
        properties.setProperty("spring.datasource.username", getDatabaseUser());
        properties.setProperty("spring.profiles.active", "dev");
        properties.setProperty("spring.jpa.hibernate.ddl-auto", "update");
        properties.setProperty("global.use_db_updater", "false");
        properties.setProperty("global.templates.encoding", "windows-1251");
        properties.setProperty("spring.datasource.url", getDatabaseUrl());
        properties.setProperty("spring.datasource.driverClassName", getDriverClass());
        properties.setProperty("spring.main.allow-bean-definition-overriding", "true");
        properties.setProperty("spring.jpa.database-platform", getDialect());
        properties.setProperty("global.messages.path", getInstallationPath() + FileSystems.getDefault().getSeparator() + "i18n");

        return properties;
    }
}
